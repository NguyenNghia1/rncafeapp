/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './js/App';
import {name as appName} from './app.json';
import OrderStatusComponent from './js/components/orderstatus/OrderStatusComponent';
import ShoppingCardComponent from './js/components/HomeComponent';
import ShippingInfoComponent from './js/components/checkout/ShippingInfoComponent';

AppRegistry.registerComponent(appName, () => App);
