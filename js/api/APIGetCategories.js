import { useState } from 'react'
import APIClient from './APIClient'
import GLOBALS from '../Global'
import Oauth1Helper from './OauthHelper'

export default () => {
    const [loading, setIsLoading] = useState(false)
    const [categoriesResponse, setCategoriesRespone] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');
    var endPoint = "/products/categories"

    const request = {
        url: GLOBALS.BASE_URL + endPoint,
        method: 'GET',
    };

    

    const callCategoryAPI = async () => {
        console.log('callCategoriessAPI');
        setIsLoading(true)
        try {
            const authHeader = Oauth1Helper.getAuthHeaderForRequest(request);
            const response = await APIClient(endPoint, { headers: authHeader });
            setCategoriesRespone(response)
            setIsLoading(false)
        } catch (err) {
            //setErrorMessage('Something went wrong');
            console.log(err)
            setErrorMessage(err)
            setIsLoading(false)
        }
    };

    return [callCategoryAPI, loading, categoriesResponse, errorMessage]
}