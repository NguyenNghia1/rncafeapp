import axios from 'axios';
import GLOBALS from '../Global'

export default axios.create({
  baseURL: GLOBALS.BASE_URL,
  timeout:20000
});