import { useState } from 'react'
import APIClient from './APIClient'
import GLOBALS from '../Global'
import Oauth1Helper from './OauthHelper'

export default (onSuccess, onError) => {
    const [loading, setIsLoading] = useState(false)
    const [response, setRespone] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');
    var endPoint = "/orders"

    const request = {
        url: GLOBALS.BASE_URL + endPoint,
        method: 'POST',
    };



    const createOrderAPI = async (order) => {
        setIsLoading(true)
        try {
            const authHeader = Oauth1Helper.getAuthHeaderForRequest(request);
            console.log(order.lineItems)
            const response = await APIClient(endPoint, {
                headers: authHeader,
                method: 'POST',
                data: {
                    "set_paid": true,
                    "billing": {
                        "first_name": order.name,
                        "address_1": order.address,
                        "email": order.email,
                        "phone": order.tel
                    },
                    "shipping": {
                        "first_name": order.name,
                        "address_1": order.address,
                    },
                    "line_items": order.lineItems
                }
            });
            console.log(response)
            setRespone(response)
            setIsLoading(false)
            onSuccess()
        } catch (err) {
            //setErrorMessage('Something went wrong');
            console.log(err)
            setErrorMessage(err)
            setIsLoading(false)
            onError()
        }
    };

    return [createOrderAPI, loading, response, errorMessage]
}