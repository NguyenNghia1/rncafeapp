// const crypto = require('crypto');
// const oauth1a = require('oauth-1.0a');

import HmacSHA1 from 'crypto-js/hmac-sha1';
import Base64 from 'crypto-js/enc-base64';
import oauth1a from 'oauth-1.0a'

const CONSUMERKEY = 'ck_aa04f3c293456d4cb14b7fc6839924fa1fbbbdbc';
const CONSUMERSECRET = 'cs_727b0de0f09a583e3ad28e8f3a1bdd5c93a79b6a';

class Oauth1Helper {
    static getAuthHeaderForRequest(request) {
        const oauth = oauth1a({
            consumer: { key: CONSUMERKEY, secret: CONSUMERSECRET },
            signature_method: 'HMAC-SHA1',
            hash_function(base_string, key) {
                return Base64.stringify(HmacSHA1(base_string, key))
            },
        })

        const authorization = oauth.authorize(request);

        return oauth.toHeader(authorization);
    }
}

module.exports = Oauth1Helper;