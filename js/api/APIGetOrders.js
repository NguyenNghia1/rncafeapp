import { useState } from 'react'
import APIClient from './APIClient'
import GLOBALS from '../Global'
import Oauth1Helper from './OauthHelper'

export default () => {
    const [loading, setIsLoading] = useState(false)
    const [ordersResponse, setOrdersResponse] = useState([]);
    const [updatedResponse, setUpdatedResponse] = useState([])
    const [errorMessage, setErrorMessage] = useState('');
    var endPoint = "/orders"

    const getRequest = {
        url: GLOBALS.BASE_URL + endPoint,
        method: 'GET',
    };
    
    const getOrderAPI = async () => {
        setOrdersResponse([])
        setIsLoading(true)
        try {
            const authHeader = Oauth1Helper.getAuthHeaderForRequest(getRequest);
            const res = await APIClient(endPoint, { headers: authHeader });
            setOrdersResponse(res)
            // console.log(resultOrders)
            setIsLoading(false)
        } catch (err) {
            console.log(err)
            setErrorMessage(err)
            setIsLoading(false)
        }
    };

    const updateStatusOrder = async (orderId) => {
        const updateRequest = {
            url: GLOBALS.BASE_URL + endPoint + `/${orderId}`,
            method: 'PUT',
        };
        console.log(orderId)
        setIsLoading(true)
        try {
            const authHeader = Oauth1Helper.getAuthHeaderForRequest(updateRequest);
            const res = await APIClient(endPoint + `/${orderId}`, {
                headers: authHeader,
                method: 'PUT',
                data: {
                    "status": "completed"
                }
            });
            console.log(res)
            setUpdatedResponse(res)
            getOrderAPI()
        } catch (err) {
            console.log(err)
            setErrorMessage(err)
            setIsLoading(false)
        }
    }

    return [getOrderAPI, updateStatusOrder, loading, ordersResponse, updatedResponse, errorMessage]
}