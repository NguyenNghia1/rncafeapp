import { useState } from 'react'
import APIClient from './APIClient'
import GLOBALS from '../Global'
import Oauth1Helper from './OauthHelper'
import axious from 'axios'

export default () => {
    const [loading, setIsLoading] = useState(false)
    const [productResponse, setProductRespone] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');
    var endPoint = "/products"

    const request = {
        url: GLOBALS.BASE_URL + endPoint,
        method: 'GET',
    };

    

    const callProductsAPI = async () => {
        console.log('callProductsAPI');
        setIsLoading(true)
        try {
            const authHeader = Oauth1Helper.getAuthHeaderForRequest(request);
            console.log(authHeader)
            console.log(GLOBALS.BASE_URL + endPoint)
            const response = await APIClient(endPoint, { headers: authHeader });
            //console.log(response)
            setProductRespone(response)
            setIsLoading(false)
        } catch (err) {
            //setErrorMessage('Something went wrong');
            console.log(err)
            setErrorMessage(err)
            setIsLoading(false)
        }
    };

    return [callProductsAPI, loading, productResponse, errorMessage]
}