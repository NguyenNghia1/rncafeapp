import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, StyleSheet, StatusBar, Image, ScrollView } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import JITTextInput from '../../common/JITTextInput';
import JITButon from '../../common/JITButton';
import JITCheckBox from '../../common/JITCheckBox';
import APICreateOrder from '../../api/APICreateOrder';
import ErrorModal from '../modal/ErrorModal'
import { TouchableOpacity } from 'react-native-gesture-handler';

let lineItems = []
const ShippingInfoComponent = (props) => {
    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [tel, setTel] = useState('');
    const [lineItems, setLineItems] = useState([])
    const [checkedFreeShip, setFreeShip] = useState(false);
    const [checkShipNow, setShipNow] = useState(false);
    const [checkNomalShip, setNomalShip] = useState(false)
    const [isShowErrorModal, setShowErrorModal] = useState(false);
    const { goBack } = props.navigation;
    const [createOrderAPI, loading, response, errorMessage] = APICreateOrder(() => onSuccess(), () => onError());

    const onError = () => {
        setShowErrorModal(errorMessage ? true : false)
    }

    const onSuccess = () => {
        props.navigation.navigate('OrderStatus')
    }

    closeErrorModal = () => {
        setShowErrorModal(false);
    }

    useEffect(() => {
        let products = props.route.params.products;
        console.log(products)
        if (products != null) {
            products.map((item) => {
                lineItems.push({ "product_id": `${item.product_id}`, "quantity": `${item.quantity}` })
            })
            setLineItems(lineItems)
        }
    }, [])

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" hidden={false} backgroundColor='transparent' translucent={true} />
            <View style={styles.header}>
                <TouchableOpacity style={styles.containerIcon} onPress={() => goBack()}>
                    <Image style={styles.imageBack} source={require('../../icon/icon_back_dark.png')} />
                </TouchableOpacity>
                <View style={{ flex: 1, alignItems: 'center', paddingEnd: 10 }}>
                    <Text style={styles.textTitle}>Shipping Information</Text>
                </View>
            </View>
            <View style={{ height: 1, marginTop: 10, backgroundColor: '#E6E9ED' }} />
            <ScrollView contentContainerStyle={styles.contentContainerScroll}
                style={styles.scroll}
                keyboardShouldPersistTaps='handled'>
                <View>
                    <JITTextInput
                        title='Email'
                        returnKeyType={"next"}
                        onChangeText={(value) => setEmail(value)}
                        blurOnSubmit={false}
                    />
                    <View
                        style={{
                            flexDirection: 'row',
                            margin: 15
                        }}>
                        <Text
                            style={{
                                color: '#919191',
                                fontFamily: 'OpenSans-Light',
                                fontSize: 16,
                            }}
                        >Already have an account?</Text>
                        <Text style={{ color: '#0D9F67', fontFamily: 'OpenSans-Bold', fontSize: 16, marginStart: 25 }}
                            onPress={() => props.navigation.navigate("Home")}
                        >Log in</Text>


                    </View>
                    <JITTextInput
                        title='Full Name'
                        returnKeyType={"next"}
                        onChangeText={(value) => setName(value)}
                        blurOnSubmit={false}
                    />
                    <JITTextInput
                        title='Street Address'
                        returnKeyType={"next"}
                        onChangeText={(value) => setAddress(value)}
                        blurOnSubmit={false}
                    />
                    <JITTextInput
                        title='Phone Number'
                        onChangeText={(value) => setTel(value)}
                    />
                </View>
                <View style={{ marginStart: 15 }}>
                    <Text style={styles.textTitle}>Shipping Method</Text>
                    <JITCheckBox
                        titleCheckbox='Free FedEx Ground shipping'
                        text='Free'
                        checked={checkedFreeShip}
                        checkedStyle={{ width: 20, height: 20, resizeMode: 'cover' }}
                        onPress={() => { setFreeShip(true), setNomalShip(false), setShipNow(false) }}
                    />
                    <JITCheckBox
                        titleCheckbox='Free FedEx Ground shipping 2-3 business hours after processing.'
                        text='$5.99'
                        checked={checkNomalShip}
                        style={{ width: '70%' }}
                        checkedStyle={{ width: 20, height: 20, resizeMode: 'cover' }}
                        onPress={() => { setFreeShip(false), setNomalShip(true), setShipNow(false) }}
                    />
                    <JITCheckBox
                        titleCheckbox='Free FedEx Now Shipping.'
                        checked={checkShipNow}
                        checkedStyle={{ width: 20, height: 20, resizeMode: 'cover' }}
                        text='$17.50'
                        onPress={() => { setFreeShip(false), setNomalShip(false), setShipNow(true) }}
                    />
                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        bottom: 0
                    }}
                >
                    <JITButon
                        style={styles.button}
                        title="Finish Checkout"
                        onPress={() => createOrderAPI({ email, name, address, tel, lineItems })}
                    />
                </View>
            </ScrollView>
            <Spinner
                visible={loading}
                textStyle={{ color: '#FFF' }}
            />
            <ErrorModal isVisible={isShowErrorModal} error={errorMessage} onBackdropPress={closeErrorModal} onPress={closeErrorModal} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: 'white'
    },
    contentContainerScroll: {
        flexGrow: 1,
        justifyContent: 'space-between',
        flexDirection: 'column'
    },
    scroll: {
        backgroundColor: 'white',
        paddingBottom: 0
    },
    header: {
        width: '100%',
        height: 65,
        flexDirection: 'row',
    },
    imageBack: {
        width: 10,
        height: 17,
    },
    textTitle: {
        color: '#666666',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    button: {
        height: 60,
        padding: 20,
        backgroundColor: '#0D9F67',
        marginTop: 15
    },
    containerIcon: {
        width: 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
    }
})

export default ShippingInfoComponent;
