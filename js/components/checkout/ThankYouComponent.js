import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, Image } from 'react-native';

const ThankYouComponent = (props) => {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" hidden={false} backgroundColor='transparent' translucent={true} />
            <View style={styles.header}>
                <Image style={styles.imageBack} source={require('../../icon/icon_back_dark.png')} onPre />
                <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
                    <Text style={styles.textTitle}>Thank You</Text>
                </View>
            </View>
            <View style={{ height: 1, marginTop: 10, backgroundColor: '#E6E9ED' }} />
            <View
                style={{
                    marginStart: 15,
                    marginEnd: 15,
                }}>
                <Text style={{
                    color: '#666666',
                    alignItems: 'center',
                    fontFamily: 'OpenSans-Bold',
                    fontSize: 22,
                    marginTop: 30,
                }}>Thank you, your order has been placed</Text>
                <Text style={{
                    color: '#919191',
                    alignItems: 'center',
                    fontFamily: 'OpenSans-Light',
                    fontSize: 16,
                    marginTop: 30,
                }}>Your reference number is {highlight('#1234abc')}</Text>
                <Text style={{
                    color: '#919191',
                    alignItems: 'center',
                    fontFamily: 'OpenSans-Light',
                    fontSize: 16,
                }}>An order confirm confirmation email has been sent to {highlight('nghianguyensoict@gmail.com')}</Text>
            </View>
        </View >
    );
}

const highlight = string =>
    string.split(' ').map((word, i) => (
        <Text key={i}>
            <Text style={styles.highlighted}>{word} </Text>
        </Text>
    ));

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: 'white'
    },
    header: {
        width: '100%',
        height: 65,
        flexDirection: 'row',
    },
    imageBack: {
        width: 10,
        height: 17,
        marginTop: 35,
        marginStart: 17
    },
    textTitle: {
        color: '#666666',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    highlighted: {
        fontFamily: 'OpenSans-Bold',
    },
})

export default ThankYouComponent;
