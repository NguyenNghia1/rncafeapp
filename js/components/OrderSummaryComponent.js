import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, Image, Dimensions, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import JITTextInput from '../common/JITTextInput';
import JITButon from '../common/JITButton';

const OrderSummaryComponent = props => {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="light-content" hidden={false} backgroundColor='transparent' translucent={true} />
            <View style={styles.header}>
                <Image style={styles.imageBack} source={require('../icon/icon_back.png')} />
                <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
                    <Text style={styles.textTitle}>Order Summary</Text>
                </View>
            </View>
            <ScrollView>
                <View style={{ flex: 1 }}>
                    <Image
                        style={{
                            width: '100%',
                            height: 270,
                            resizeMode: 'stretch'
                        }}
                        source={require('../icon/bg_image_order.png')} />
                    <View
                        style={{
                            margin: 20
                        }}>
                        <Text
                            style={{
                                color: '#666666',
                                fontSize: 20,
                                fontFamily: 'OpenSans-Semibold'
                            }}
                        >Chicken Salad</Text>
                        <JITTextInput
                            title="Base Price"
                            value="$12.00"
                            editable={false}
                            disabled={true}
                        />
                        <JITTextInput
                            title="Quantity"
                            value="1"
                            editable={false}
                            disabled={true}
                        />
                        <JITTextInput
                            title="Size"
                            value="Large"
                            editable={false}
                            disabled={true}
                        />
                        <JITTextInput
                            title="Tax"
                            value="$1.20"
                            editable={false}
                            disabled={true}
                        />
                        <JITTextInput
                            title="Subtotal"
                            value="$13.20"
                            editable={false}
                            disabled={true}
                        />
                        <View>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginStart: 15,
                                    marginEnd: 15,
                                    alignItems: "center",
                                    justifyContent: 'space-between',
                                    height: 62
                                }}
                            >
                                <Text style={styles.text}>Total</Text>
                                <Text style={{
                                    color: '#0D9F67',
                                    fontSize: 18,
                                    fontFamily: 'OpenSans-Semibold'
                                }}>$13.20</Text>

                            </View>
                            <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
                        </View>
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                        }}>
                            <JITButon
                                style={styles.button}
                                title="Confirm The Order"
                            />
                            <TouchableOpacity
                                style={styles.buttonCancel}>
                                <Text style={{
                                    fontSize: 16,
                                    color: "#C7C7C7",
                                    textAlign: 'center'
                                }}>Add to favorites</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </ScrollView>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
    },
    header: {
        width: '100%',
        height: 65,
        backgroundColor: '#DBC8A8',
        flexDirection: 'row',
    },
    imageBack: {
        width: 10,
        height: 17,
        marginTop: 35,
        marginStart: 17
    },
    textTitle: {
        color: 'white',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    text: {
        color: '#666666',
        fontSize: 18,
        fontFamily: 'OpenSans-Semibold'
    },
    button: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0D9F67',
        marginTop: 15
    },
    buttonCancel: {
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        borderWidth: 1,
        borderColor: '#E6E9ED'

    }
})

export default OrderSummaryComponent;
