import React, { useEffect } from 'react';
import { View, Text, StatusBar, StyleSheet, Image, TouchableWithoutFeedback, ScrollView, TouchableOpacity } from 'react-native';

const OrderDetailComponent = (props) => {

    const orderDetail = props.route.params.item ?? null;
    return (
        <ScrollView>
            <View style={styles.container}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor='transparent' translucent={true} />
                <View style={styles.header}>
                    <TouchableWithoutFeedback style={styles.containerIcon}  onPress={() => { props.navigation.goBack() }}><Image style={styles.imageBack} source={require('../icon/icon_back_dark.png')} /></TouchableWithoutFeedback>
                    <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
                        <Text style={styles.textTitle}>Thông tin đơn hàng</Text>
                    </View>
                </View>

                <View style={{ backgroundColor: 'white', marginTop: 12, borderTopWidth: 1, borderTopColor: '#e7e7e7', flexDirection: 'row', paddingVertical: 21 }}>
                    <Image style={{ marginLeft: 24, width: 16, height: 22 }} source={require('../../assets/icon/ic_gps.png')} />
                    <View style={{ marginLeft: 24 }}>
                        <Text style={styles.headerText}>Địa chỉ nhận hàng</Text>
                        <Text style={styles.contentText}>{orderDetail.shipping.first_name}</Text>
                        <Text style={styles.contentText}>{orderDetail.billing.phone}</Text>
                        <Text style={{...styles.contentText,...{marginRight:48}}}>{orderDetail.shipping.address_1}</Text>
                    </View>
                </View>
                <View style={{ backgroundColor: 'white', borderTopWidth: 1, borderTopColor: '#e7e7e7', flexDirection: 'row', paddingVertical: 21 }}>
                    <Image style={{ marginLeft: 24, width: 24, height: 18 }} source={require('../../assets/icon/ic_truck.png')} />
                    <View style={{ marginLeft: 24 }}>
                        <Text style={styles.headerText}>Thông tin vận chuyển</Text>
                        <Text style={styles.contentText}>Giao Hàng Nhanh</Text>
                    </View>
                </View>
                <View style={{
                    backgroundColor: 'white', borderTopWidth: 1, borderTopColor: '#e7e7e7', borderBottomWidth: 1, borderBottomColor: '#e7e7e7',
                    flexDirection: 'row', paddingVertical: 21
                }}>
                    <Image style={{ marginLeft: 24, width: 18, height: 15 }} source={require('../../assets/icon/ic_money.png')} />
                    <View style={{ marginLeft: 24, flexGrow: 1 }}>
                        <Text style={styles.headerText}>Thông tin thanh toán</Text>
                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <Text style={styles.paymentContentText}>Tổng tiên hàng</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.paymentAmountText}>đ {orderDetail.total}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <Text style={styles.paymentContentText}>Vận chuyển</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={styles.paymentAmountText}>đ 10.000</Text>
                        </View>
                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <Text style={{ color: '#000000', fontSize: 17, fontFamily: 'OpenSans-Regular' }}>Tổng số tiền</Text>
                            <View style={{ flex: 1 }} />
                            <Text style={{ color: '#ee4d2d', fontSize: 18, marginRight: 24 }}>đ {parseFloat(orderDetail.total) + 10}</Text>
                        </View>
                    </View>
                </View>

                <View style={{
                    backgroundColor: 'white', borderTopWidth: 1, borderTopColor: '#e7e7e7', borderBottomWidth: 1, borderBottomColor: '#e7e7e7',
                    flexDirection: 'row', paddingVertical: 12, marginTop: 12
                }}>
                    <Image style={{ width: 15, height: 15, marginLeft: 24 }}
                        source={{ uri: `http://wpinternal.jit.vn/wp-content/uploads/2020/04/C381o-Alan-Walker-TrE1BABB-Em-10-tuE1BB95i.jpg` }} />
                    <Text style={{ fontSize: 12, color: '#919191', fontFamily: 'OpenSans-Light', alignSelf: 'center', marginLeft: 6 }}>Nghia Nguyen</Text>
                </View>

                <View style={{ flexDirection: 'row', paddingVertical: 18, width: '100%' }}>
                    <Image style={{ width: 80, height: 80, marginLeft: 24 }}
                        source={{ uri: `http://wpinternal.jit.vn/wp-content/uploads/2020/04/C381o-Alan-Walker-TrE1BABB-Em-10-tuE1BB95i.jpg` }} />
                    <View style={{ flexDirection: 'column', flexGrow: 1, marginLeft: 24 }}>
                        <Text style={styles.headerText}>T-shirt</Text>
                        <Text style={styles.contentText}>Black</Text>
                        <View style={{ flexDirection: 'row', width: '100%' }}>
                            <View style={{ flex: 1 }} />
                            <Text style={{ color: '#ee4d2d', fontSize: 18, marginRight: 24 }}>đ {orderDetail.total}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    backgroundColor: 'white', borderTopWidth: 2, borderTopColor: '#e8e8e8', borderBottomWidth: 2, borderBottomColor: '#e8e8e8',
                    paddingVertical: 12, width: '100%', paddingLeft: 24, paddingRight: 24
                }}>
                    <View style={{ flexDirection: 'row', flexGrow: 1}}>
                        <Text style={styles.headerText}>ID Đơn hàng</Text>
                        <View style={{ flexGrow: 1 }} />
                        <Text style={{ ...styles.headerText, ...{ fontFamily: 'OpenSans-Semibold' } }}>2SFJJAA52</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flexGrow: 1}}>
                        <Text style={styles.contentText}>Thời Gian Đặt Hàng</Text>
                        <View style={{ flexGrow: 1 }} />
                        <Text style={styles.contentText}>24-04-2020 9:34</Text>
                    </View>
                    <View style={{ flexDirection: 'row', flexGrow: 1}}>
                        <Text style={styles.contentText}>Thời Gian Thanh Toán</Text>
                        <View style={{ flexGrow: 1 }} />
                        <Text style={styles.contentText}>24-04-2020 9:34</Text>
                    </View>
                </View>
                <TouchableOpacity>
                    <View style={{
                        flexDirection: 'row', flexGrow: 1, height: 52, marginVertical: 18, marginHorizontal: 24,
                        borderColor: '#b6b6b6', borderWidth: 1, borderRadius: 6, justifyContent: 'center', alignItems: 'center'
                    }}>
                        <Text>Hủy</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5'
    },
    header: {
        width: '100%',
        height: 65,
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    textTitle: {
        color: '#666666',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    imageBack: {
        width: 10,
        height: 17,
    },
    containerIcon: {
        width: 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
    },
    headerText: { color: '#212121', fontSize: 16, fontFamily: 'OpenSans-Regular' },
    contentText: { color: '#595959', fontSize: 16, fontFamily: 'OpenSans-Light'},
    paymentContentText: { color: '#000000', fontSize: 16, fontFamily: 'OpenSans-Light' },
    paymentAmountText: { color: '#000000', fontSize: 16, fontFamily: 'OpenSans-Light', marginRight: 24 }
})

export default OrderDetailComponent;