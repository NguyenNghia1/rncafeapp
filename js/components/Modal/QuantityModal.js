import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import Modal from 'react-native-modal';

const QuantityModal = props => {

    generateQuantity = () => {
        return Array.from({ length: 10 }, (v, k) => k + 1);
    }
    return (
        <Modal
            isVisible={props.isVisible}
            onBackdropPress={props.onBackdropPress}
            backdropColor='#555656'
        >
            <View style={styles.content}>
                <Text style={{
                    textAlign: 'center',
                    marginTop: 15,
                    marginBottom: 15,
                    color: '#666666',
                    textAlign: "center",
                    fontSize: 18,
                    fontFamily: 'OpenSans-BoldItalic'
                }}>Please Choose Quantity!</Text>
                <FlatList
                    data={generateQuantity()}
                    renderItem={({ item }) => (
                        <TouchableWithoutFeedback onPress={() => {
                            props.onClickItem(item);
                        }}>
                            <View style={{ width: 300 }}>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        margin: 15,
                                        color: '#919191',
                                        textAlign: "center",
                                        fontSize: 18,
                                        fontFamily: 'OpenSans-Light'
                                    }}>{item}</Text>
                                <View style={{ justifyContent: 'flex-end', }}>
                                    <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    )}
                    keyExtractor={(item => item)}
                >

                </FlatList>
            </View>

        </Modal>
    );
}
const styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        height: 300,
        width: 300,
        alignSelf: "center"
    },
});

export default QuantityModal;
