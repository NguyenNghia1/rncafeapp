import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';

const screen = Dimensions.get('window');
const ErrorModal = props => {
    return (
        <Modal
            style={{
                justifyContent: 'center', alignItems: 'center', borderRadius: Platform.OS === 'ios' ? 30 : 0, shadowRadius: 10,
                width: screen.width - 80, height: 280
            }}
            isVisible={props.isVisible}
            onBackdropPress={props.onBackdropPress}
            backdropColor='#555656'
        >
            <View style={{
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 4,
                borderColor: 'rgba(0, 0, 0, 0.1)',
                height: 200,
                width: 300,
                alignSelf: "center"
            }}>
                <Text style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginBottom: 40
                }}>{props.error.toString()}</Text>
                <TouchableOpacity
                    style={{
                        padding: 8,
                        width: 100,
                        height: 40,
                        borderRadius: 6,
                        backgroundColor: 'mediumseagreen',
                        alignItems: 'center'
                    }}
                    onPress={props.onPress}
                    activeOpacity={1}>
                    <Text style={{ fontSize: 18, color: 'white' }}>Oke</Text>
                </TouchableOpacity>
            </View>
        </Modal>
    );
}

export default ErrorModal;
