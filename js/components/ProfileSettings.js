import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, StatusBar, Image } from 'react-native';
import JITTextInput from '../common/JITTextInput';
import JITSwitch from '../common/JITSwitch';

const ProfileSettings = props => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <StatusBar barStyle="light-content" hidden={false} backgroundColor='transparent' translucent={true} />
        <View style={styles.header}>
          <View>
            <Image style={styles.iconMenu} source={require('../icon/icon_navigation_white.png')} />
          </View>
          <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
            <Text style={styles.textTitle}>Settings</Text>
          </View>
          <View >
            <Image style={styles.imageSettings} source={require('../icon/icon_settings.png')} />
          </View>
        </View>
        <View style={styles.avartar} >
          <Image
            style={{
              width: 160,
              height: 160,
              borderRadius: 160 / 2,
              resizeMode: 'cover',
              position: 'absolute'
            }}
            source={require('../icon/image_user.png')} />
          <Image style={styles.imageCamera} source={require('../icon/icon_cam.png')} />
        </View>
        <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />

        <JITTextInput
          title='Name'
          value='Alexander Karev'
        />
        <JITTextInput
          title='Title'
          value='UI Designer Coffee Lover'
        />
        <JITTextInput
          title='Email'
          value='alexander@five.angency'
        />
        <JITTextInput
          title='Password'
          value='AlexanderKarev'
          secureTextEntry={true}
        />
        <JITTextInput
          title='Name'
          value='Alexander Karev'
        />
        <JITSwitch
          title='Facebook'
        />
        <JITSwitch
          title='Notifications'
          value={true}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  header: {
    height: 65,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch',
    paddingStart: 17,
    paddingTop: 25,
    paddingEnd: 17,
    backgroundColor: '#DBC8A8',
  },
  iconMenu: {
    width: 20,
    height: 14,
    marginTop: 5
  },
  imageSettings: {
    width: 18,
    height: 18,
    marginTop: 5
  },
  textTitle: {
    color: 'white',
    alignItems: 'center',
    fontFamily: 'OpenSans-Bold',
    fontSize: 18,
  },
  avartar: {
    width: 160,
    height: 160,
    marginTop: 50,
    borderRadius: 160 / 2,
    backgroundColor: '#E3E3E3',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50
  },
  imageCamera: {
    height: 60,
    width: 60,
    resizeMode: 'cover'
  },
})

export default ProfileSettings;
