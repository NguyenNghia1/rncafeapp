import React, { useEffect, useState, Fragment } from 'react';
import { View, Text, FlatList, StyleSheet, Image, TouchableWithoutFeedback } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import APIGetOrders from '../../api/APIGetOrders';
import JITButon from '../../common/JITButton';

const ProsessingOrderComponent = props => {

    renderViewExtra = (item) => {
        if (item.line_items.length > 1) {
            return (
                <View>
                    <View style={{ alignItems: 'center', padding: 10 }}>
                        <Text style={styles.text}>Xem thêm {item.line_items.length - 1} sản phẩm</Text>
                    </View>
                    <View style={{ height: 1, backgroundColor: '#E6E9ED', }} />
                </View>
            )
        }
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: '#F3F3F3',
        }}>
            <FlatList
                data={props.processingOrders}
                renderItem={({ item, index }) => (
                    <TouchableWithoutFeedback onPress={() => props.onClickItem(item)}>
                        <View style={{ backgroundColor: 'white', marginTop: 5, padding: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 15, height: 15, marginEnd: 5 }}
                                        source={{ uri: `http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_${(index % 8) ? (index % 8) : 3}_front.jpg` }} />
                                    <Text style={styles.text}>{item.billing.first_name}</Text>
                                </View>
                                <Text style={styles.highlightText}>Hoàn thành</Text>
                            </View>

                            <View style={{
                                flexDirection: 'row', marginTop: 15, justifyContent: 'space-between',
                            }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 50, height: 50, marginEnd: 10 }} source={{ uri: `http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_${(index % 8) ? (index % 8) : 3}_front.jpg` }} />
                                    <Text style={{
                                        fontSize: 12, color: '#666666', fontFamily: 'OpenSans-Light',
                                        marginBottom: 5,
                                    }}>Tên sản phẩm:  {item.line_items[0].name ? item.line_items[0].name : 'Áo thun gia đình mùa hè'}</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'column'
                                }}>
                                    <Text style={styles.text}>SL: {item.line_items[0].quantity}</Text>
                                    <Text style={styles.highlightText}>{(item.line_items[0].total !== "0.00") ? item.line_items[0].total : '16.0'}</Text>
                                </View>
                            </View>
                            <View style={{ height: 1, backgroundColor: '#E6E9ED', marginTop: 5 }} />
                            {/* <View style={{ alignItems: 'center', padding: 10 }}>
                            <Text style={{}}>B</Text>
                        </View>
                        <View style={{ height: 1, backgroundColor: '#E6E9ED', }} /> */}
                            {renderViewExtra(item)}
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                padding: 10
                            }}>
                                <Text style={{
                                    fontSize: 12,
                                    color: '#919191',
                                    fontFamily: 'OpenSans-Light',
                                    marginTop: 7,
                                    marginBottom: 5
                                }}>{item.line_items.length} sản phẩm</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image style={{ width: 30, height: 30, resizeMode: 'cover', marginEnd: 20 }} source={require('../../../assets/icon/icon_security.png')} />
                                    <Text style={{
                                        fontSize: 12,
                                        color: '#0D9F67',
                                        fontFamily: 'OpenSans-Light',
                                        marginTop: 7,
                                        marginBottom: 5
                                    }}>Tổng thanh toán : {item.total}</Text>
                                </View>
                            </View>
                            <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                padding: 10
                            }}>
                                <Text style={{ ...styles.text }}>Bạn chưa nhận được hàng?</Text>
                                <JITButon
                                    style={{ width: 100, height: 30, backgroundColor: '#0D9F67', borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}
                                    textStyle={{ fontSize: 14, color: 'white' }}
                                    title='Đã nhận'
                                    onPress={() => props.updateOrderStatus(item.id)}
                                />
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                )}
            >
            </FlatList>
        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        fontSize: 12,
        color: '#919191',
        fontFamily: 'OpenSans-Light',
        marginBottom: 5,
        alignSelf: 'center'
    },
    highlightText: {
        fontSize: 12,
        color: '#0D9F67',
        fontFamily: 'OpenSans-Light'
    }
})

export default ProsessingOrderComponent;
