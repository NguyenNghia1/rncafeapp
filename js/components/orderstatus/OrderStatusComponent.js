import React, { useState, useEffect } from 'react';
import { View, Text, StatusBar, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import ProcessingOrderComponent from './ProsessingOrderComponent'
import CompletedOrderComponent from './CompletedOrderComponent';
import APIGetOrders from '../../api/APIGetOrders';
import Spinner from 'react-native-loading-spinner-overlay';

const OrderStatusComponent = (props) => {

    const [val, setVal] = useState(1);
    const [completedOrders, setCompletedOrders] = useState([]);
    const [processingOrders, setProcessingOrders] = useState([]);
    const [getOrderAPI, updateStatusOrder, loading, ordersResponse, updatedResponse, errorMessage] = APIGetOrders()

    const getOrdersSuccess = () => {
        console.log("getOrdersSuccess")
        if (processingOrders.length > 0) {
            setProcessingOrders([])
        }
        if (completedOrders.length > 0) {
            setCompletedOrders([]);
        }
        if (ordersResponse.data != null) {
            ordersResponse.data.map((item) => {
                if (item.status === "processing") {
                    processingOrders.push(item)
                } else if (item.status === "completed") {
                    completedOrders.push(item)
                }
            })
            setProcessingOrders(processingOrders);
            setCompletedOrders(completedOrders)

        }
    }

    const updateOrderSuccess = () => {
        console.log("updateOrderSuccess")
        if (updatedResponse.status === 200) {
            setVal(2)
        }
    }

    const onError = () => {
        console.log("error")
    }

    useEffect(() => {
        console.log("Call API")
        getOrderAPI()
    }, [])

    useEffect(() => {
        getOrdersSuccess()
    }, [ordersResponse])

    useEffect(() => {
        if (updatedResponse.length !== 0) {
            updateOrderSuccess();
        }
    }, [updatedResponse])

    const onClickItem = (item) => {
        props.navigation.navigate("OrderDetail", { item })
    }

    const gotoHome = () => {
        props.navigation.navigate("Home")
    }

    const renderElement = () => {
        if (val === 1) {
            return <ProcessingOrderComponent processingOrders={processingOrders} updateOrderStatus={updateStatusOrder} onClickItem={onClickItem} />
        } else {
            return <CompletedOrderComponent completedOrders={completedOrders} gotoHome={gotoHome} />
        }
    }
    return (
        <View style={{
            flex: 1,
            backgroundColor: 'white'
        }}>
            <StatusBar barStyle="dark-content" hidden={false} backgroundColor='transparent' translucent={true} />
            <View style={styles.header}>
                <TouchableOpacity style={styles.containerIcon} onPress={() => props.navigation.dispatch(DrawerActions.openDrawer())} activeOpacity={1}>
                    <Image style={styles.iconMenu} source={require('../../icon/icon_navigation.png')} />
                </TouchableOpacity>
                <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
                    <Text style={styles.textTitle}>Đơn hàng</Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', height: 70 }}>
                <TouchableOpacity
                    style={{ width: '50%', alignItems: "center", justifyContent: 'center' }}
                    activeOpacity={1}
                    onPress={() => setVal(1)}
                >
                    <Image style={styles.image} source={require('../../../assets/icon/icon_processing.png')} />
                    <Text style={{
                        fontSize: 14,
                        color: '#919191',
                        fontFamily: 'OpenSans-Light',
                        marginTop: 7,
                        marginBottom: 5
                    }}>Đang Giao</Text>
                    <View style={{
                        width: '100%',
                        height: 2,
                        backgroundColor: (val === 1) ? '#0D9F67' : '#E6E9ED'
                    }} />
                </TouchableOpacity>
                <TouchableOpacity
                    style={{ width: '50%', alignItems: "center", justifyContent: 'center' }}
                    activeOpacity={1}
                    onPress={() => setVal(2)}
                >
                    <Image style={styles.image} source={require('../../../assets/icon/icon_completed.png')} />
                    <Text style={{
                        fontSize: 14,
                        color: '#919191',
                        fontFamily: 'OpenSans-Light',
                        marginTop: 7,
                        marginBottom: 5
                    }}>Đã Giao</Text>
                    <View style={{
                        width: '100%',
                        height: 2,
                        backgroundColor: (val === 2) ? '#0D9F67' : '#E6E9ED'
                    }} />
                </TouchableOpacity>
            </View>
            {renderElement()}

            {/* <Spinner 
                visible={loading}
                textStyle={{ color: '#FFF' }}/> */}

        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 80,
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    iconMenu: {
        width: 20,
        height: 14,
    },
    textTitle: {
        color: '#666666',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    image: {
        width: 40,
        height: 40,
        resizeMode: 'cover'
    },
    text: {
        fontSize: 14,
        color: '#919191',
        fontFamily: 'OpenSans-Light',
        marginTop: 7,
    },
    containerIcon: {
        width: 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25
    }
})

export default OrderStatusComponent;
