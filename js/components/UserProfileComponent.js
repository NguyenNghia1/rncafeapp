import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, TouchableOpacity, Dimensions } from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import JITButon from '../common/JITButton';

const screenHight = Dimensions.get('window').height;
const UserProfileComponent = props => {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="light-content" hidden={false} backgroundColor='transparent' translucent={true} />
            <View
                style={{
                    flex: 8.2,
                }}>
                <Image style={{ height: '100%', width: '100%', resizeMode: 'cover', position: 'absolute' }}
                    source={require('../icon/image_profile.png')} />
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => props.navigation.dispatch(DrawerActions.openDrawer())} activeOpacity={1}>
                        <Image style={styles.imageNavigation} source={require('../icon/icon_navigation_white.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text style={styles.textTitle}>Profile</Text>
                    </View>
                    <View >
                        <Image style={styles.imageSettings} source={require('../icon/icon_settings.png')} />
                    </View>

                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        bottom: 75
                    }}
                >
                    <Text style={{
                        fontSize: 28,
                        fontFamily: 'OpenSans-Semibold',
                        color: 'white',
                        textAlign: 'center'
                    }}>Kate Hollmann</Text>
                    <Text style={{
                        fontSize: 14,
                        fontFamily: 'OpenSans-Light',
                        color: 'white',
                        textAlign: 'center'
                    }}>UI Designer {'&'} Coffee Lover</Text>

                </View>
            </View>
            <View
                style={{
                    flex: 1.8,
                    backgroundColor: 'white',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginStart: 30,
                    marginEnd: 30,

                }}
            >
                <View style={styles.textContainer}>
                    <Text style={styles.number}>0</Text>
                    <Text style={styles.text}>Post</Text>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.number}>30</Text>
                    <Text style={styles.text}>Likes</Text>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.number}>25</Text>
                    <Text style={styles.text}>Followers</Text>
                </View>
                <View style={styles.textContainer}>
                    <Text style={styles.number}>60</Text>
                    <Text style={styles.text}>Following</Text>
                </View>
            </View>
            <View style={{ width: '100%', bottom: (screenHight * 0.18 - 25), position: 'absolute' }}>
                <JITButon
                    style={{
                        width: 200,
                        height: 50,
                        padding: 12,
                        backgroundColor: '#0D9F67',
                        alignSelf: 'center',
                        borderRadius: 5
                    }}
                    title="Follow"
                />
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
    },
    header: {
        height: 65,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        marginStart: 17,
        marginTop: 35,
        marginEnd: 17,
    },
    imageNavigation: {
        width: 20,
        height: 14,
    },
    imageSettings: {
        width: 18,
        height: 18
    },
    textTitle: {
        color: 'white',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
    },
    textContainer: {
        alignItems: 'center',
        marginTop: 20
    },
    number: {
        color: '#B7B7B7',
        fontSize: 24,
        fontFamily: 'OpenSans-Light'
    },
    text: {
        color: '#B7B7B7',
        fontSize: 12,
        fontFamily: 'OpenSans-Light'
    }
})

export default UserProfileComponent; 