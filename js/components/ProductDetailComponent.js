import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, StatusBar, Image, Dimensions, TouchableOpacity } from 'react-native';
import JITButon from '../common/JITButton';
import { ScrollView } from 'react-native-gesture-handler';
import QuantityModal from './modal/QuantityModal';
import SizeModal from './modal/SizeModal';

const screenWidth = Dimensions.get('window').width;
const ProductDetailComponent = props => {

  const [quantity, setQuantity] = useState(1);
  const [size, setSize] = useState('L');
  const sizeModalRef = useRef(null);
  const quantityModalRef = useRef(null);
  const [isShowSizeModal, setShowSizeModal] = useState(false);
  const [isShowQuantityModal, setShowQuantityModal] = useState(false);
  const { goBack } = props.navigation;

  updateQuantity = (q) => {
    setQuantity(q)
    setShowQuantityModal(false)
  }

  updateSize = (s) => {
    setSize(s)
    setShowSizeModal(false)
  }

  closeSizeModal = () => {
    setShowSizeModal(false)
  }
  closeQuantityModal = () => {
    setShowQuantityModal(false)
  }

  const product = props.route.params?.product ?? null;

  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
      <ScrollView>
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" hidden={false} backgroundColor='transparent' translucent={true} />
          <View style={styles.header}>
            <TouchableOpacity style={styles.containerIcon} activeOpacity={0.7} onPress={() => goBack()}>
              <Image style={styles.imageBack} source={require('../icon/icon_back_dark.png')} />
            </TouchableOpacity>
            <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
              <Text style={styles.textTitle}>{product.name}</Text>
            </View>
          </View>
          <Image style={{ width: '100%', height: 200, resizeMode: 'contain' }} source={{ uri: product.images[0].src }} />
          <View
            style={{
              marginStart: 25,
              marginTop: 30,
              marginEnd: 25,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
            <Text
              style={{
                color: '#666666',
                fontFamily: 'OpenSans-Semibold',
                fontSize: 18,
                justifyContent: 'flex-start'
              }}>{product.name}</Text>
            <Text
              style={{
                color: '#0D9F67',
                fontSize: 18,
                fontFamily: 'OpenSans-Semibold',
                justifyContent: 'flex-end'
              }}>${product.price}</Text>
          </View>
          <View
            style={{
              margin: 25
            }}>
            <Text
              style={{
                color: '#919191',
                textAlign: "center",
                fontSize: 14,
                fontFamily: 'OpenSans-Light'
              }}>{product.description.substring(3, product.description.length - 5)}</Text>
          </View>
          <View style={{ height: 1, backgroundColor: '#E6E9ED', marginTop: 5 }} />
          <View style={{ flexDirection: 'row', width: '100%' }}>
            <TouchableOpacity
              style={{ width: '50%', marginTop: 20, alignItems: "center" }}
              onPress={() => setShowQuantityModal(true)}
              activeOpacity={1}>
              <Text style={styles.textNomal}>Quantity</Text>
              <View style={{
                flexDirection: 'row',
                marginTop: 15,
              }}>
                <Text
                  style={styles.textLarge}>{quantity}</Text>
                <Image
                  style={{
                    width: 15,
                    height: 28,
                    marginStart: 25,
                    resizeMode: 'contain'
                  }}
                  source={require('../icon/icon_arrow_down.png')} />
              </View>
            </TouchableOpacity>
            <View
              style={{
                width: 1,
                height: 110,
                backgroundColor: '#E6E9ED'
              }}
            />
            <TouchableOpacity
              style={{ width: '50%', marginTop: 20, alignItems: "center" }}
              onPress={() => setShowSizeModal(true)}
              activeOpacity={1}>
              <Text style={styles.textNomal}>Size</Text>
              <View style={{
                flexDirection: 'row',
                marginTop: 15,
              }}>
                <Text
                  style={styles.textLarge}>{size}</Text>
                <Image
                  style={{
                    width: 15,
                    height: 28,
                    marginStart: 25,
                    resizeMode: 'contain'
                  }}
                  source={require('../icon/icon_arrow_down.png')} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
          <View style={{ alignItems: 'center', marginBottom: 15 }}>
            <JITButon
              style={styles.button}
              title="Place The Order"
              onPress={() => props.navigation.navigate("ShippingInfo", { products: [{ "product_id": product.id, "quantity": quantity }] })}
            />
            <JITButon
              style={styles.buttonFavorites}
              textStyle={{ fontSize: 16, color: "#C7C7C7", textAlign: 'center' }}
              title='Add To Cart'
              onPress={() => props.navigation.navigate("ShoppingCart")} />
          </View>
        </View>
        <QuantityModal ref={quantityModalRef} onClickItem={updateQuantity} isVisible={isShowQuantityModal} onBackdropPress={closeQuantityModal} />
        <SizeModal ref={sizeModalRef} onClickItem={updateSize} isVisible={isShowSizeModal} onBackdropPress={closeSizeModal} />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: 'white'
  },
  header: {
    width: '100%',
    height: 65,
    flexDirection: 'row',
  },
  imageBack: {
    width: 10,
    height: 17,
  },
  textTitle: {
    color: '#666666',
    alignItems: 'center',
    fontFamily: 'OpenSans-Bold',
    fontSize: 18,
    marginTop: 30
  },
  textNomal: {
    color: '#919191',
    textAlign: "center",
    fontSize: 14,
    fontFamily: 'OpenSans-Light'
  },
  textLarge: {
    color: '#919191',
    textAlign: "center",
    fontSize: 18,
    fontFamily: 'OpenSans-Light'
  },
  button: {
    width: screenWidth - 50,
    height: 60,
    padding: 20,
    backgroundColor: '#0D9F67',
    marginTop: 15
  },
  buttonFavorites: {
    width: screenWidth - 50,
    height: 60,
    padding: 17,
    marginTop: 15,
    borderWidth: 1,
    borderColor: '#E6E9ED',
    backgroundColor: 'white'
  },
  containerIcon: {
    width: 40,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
  }
})

export default ProductDetailComponent;
