import React, { useState, useRef } from 'react';
import { View, StatusBar, StyleSheet, Image, Keyboard, Dimensions, TouchableWithoutFeedback, ScrollView, AsyncStorage } from 'react-native';
import JITTextInput2 from '../common/JITTextInput2';
import JITButon2 from '../common/JITButton2';
import GLOBALS from '../Global'
import UTILS from '../Utils'
import { CommonActions } from '@react-navigation/native';

const screenWidth = Dimensions.get('window').width;
const SignInComponent = props => {

    const [emailText, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const login = async () => {
        if (emailText.trim().length == 0) {
            alert('Xin bạn vui lòng nhập email')
            return;
        } else if (password.trim().length == 0) {
            alert('Xin bạn vui lòng nhập mật khẩu')
            return;
        } else if (!UTILS.ValidateEmail(emailText)) {
            alert('email bạn nhập không hợp lệ')
            return;
        }
        try {
            await AsyncStorage.setItem('email', emailText);
        } catch (error) {
            // Error saving data
        }
        var resetAction = CommonActions.reset({
            index: 0,
            routes: [
                { name: 'Home' },
            ]
        })
        props.navigation.dispatch(resetAction);
    }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.container}>
                <Image style={styles.logo} source={require('../icon/logo_dark.png')} />

                <View style={{ marginTop: 72 }}>
                    <JITTextInput2
                        title="Email"
                        blurOnSubmit={false}
                        returnKeyType={'next'}
                        onChangeText={text => setEmail(text)}
                        onSubmitEditing={() => passcode2.focus()}
                        style={styles.input}
                    />
                    <View style={{height:12}}/>
                    <JITTextInput2
                        inputRef={(r) => { passcode2 = r }}
                        title="Password"
                        onChangeText={text => setPassword(text)}
                        secureTextEntry={true}
                        style={styles.input}
                    />
                    <View style={{height:12}}/>
                    <JITButon2
                        style={styles.button}
                        title="Sign In"
                        onPress={login}
                    />
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    logo: {
        marginTop: 100,
        width: 125,
        height: 125,
        alignSelf: 'center'
    },
    button: {
        backgroundColor: 'green',
        marginHorizontal:20
    },
    butonText: {
        color: 'white',
        alignSelf: 'center',
        fontSize: 18,
        fontFamily: "OpenSans-Semibold"
    },
    input: {
        borderWidth: 1,
        borderColor: '#c3c3c3',
        textAlign: 'center',
        marginHorizontal:20
    }
})

export default SignInComponent;
