import React, { Component } from 'react';
import { View, Text, ScrollView, Image, StyleSheet, AsyncStorage } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import HomeComponent from '../HomeComponent'
import { TouchableOpacity } from 'react-native-gesture-handler';
import JITButon from '../../common/JITButton';
import OrderStatusComponent from '../orderstatus/OrderStatusComponent';
import UserProfileComponent from '../UserProfileComponent';


global.currentScreenIndex = 0;
const Drawer = createDrawerNavigator();


function customDrawerContent(props) {

    const logout = async() =>{
        try {
            await AsyncStorage.setItem('email', '');
            global.email = ''
            props.navigation.navigate('Welcome')
        } catch (error) {
            // Error saving data
        }
    }

    let items = [
        {
            navOptionThumb: 'home',
            navOptionName: 'Trang Chủ',
            screenToNavigate: 'Home',
        },
        {
            navOptionThumb: 'coffee',
            navOptionName: 'Đơn Hàng',
            screenToNavigate: 'OrderStatus',
        },
        {
            navOptionThumb: 'shopping-basket',
            navOptionName: 'Trang Cá Nhân',
            screenToNavigate: 'Profile',
        },
    ];
    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.containerImage}>
                    <Image style={styles.image} source={require('../../icon/logo_dark.png')} />
                </View>
                <View style={{ height: 1, backgroundColor: '#D8D8D8' }} />
                <View style={{ width: '100%' }}>
                    {items.map((item, key) => (
                        <TouchableOpacity
                            style={styles.containerItem}
                            activeOpacity={1}
                            onPress={() => {
                                global.currentScreenIndex = key;
                                props.navigation.navigate(item.screenToNavigate);
                            }}
                            key={key}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={styles.containerIcon}>
                                    <Icon name={item.navOptionThumb} size={25} color={global.currentScreenIndex === key ? "#0D9F67" : "#B7B7B7"} />
                                </View>
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: 'black',
                                        marginStart: 10,
                                        marginTop: 15,
                                        fontSize: 18,
                                        fontFamily: 'OpenSans-Light',
                                        color: global.currentScreenIndex === key ? "#0D9F67" : "#B7B7B7",

                                    }}>
                                    {item.navOptionName}
                                </Text>
                            </View>
                            <View style={{ height: 1, backgroundColor: '#D8D8D8' }} />
                        </TouchableOpacity>
                    ))}
                </View>
            </ScrollView>
            <JITButon
                style={styles.button}
                title="Logout"
                onPress={logout}
            />
        </View>
    )
}

const CustomDrawer = props => {
    return (
        <Drawer.Navigator drawerContent={(props) => customDrawerContent(props)}>
            <Drawer.Screen name="Home" component={HomeComponent} />
            <Drawer.Screen name="OrderStatus" component={OrderStatusComponent} />
            <Drawer.Screen name="Profile" component={UserProfileComponent} />
        </Drawer.Navigator>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 60
    },
    containerImage: {
        height: 90,
        marginBottom: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        width: 90,
        height: 90
    },
    containerItem: {
        flexDirection: 'column',
        paddingTop: 15,
        paddingBottom: 10,
        backgroundColor: '#ffffff'
    },
    containerIcon: {
        marginRight: 10,
        marginLeft: 20
    },
    button: {
        width: '100%',
        height: 50,
        padding: 12,
        backgroundColor: '#0D9F67',
        marginTop: 3
    }
})

export default CustomDrawer;
