import React, { Component, useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity, StatusBar, TouchableWithoutFeedback, AsyncStorage } from 'react-native';
import JITButton from '../common/JITButton'
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { CommonActions } from '@react-navigation/native';

const screenWidth = Dimensions.get('window').width;



const WelcomComponent = props => {

    //LoginManager.logOut()

    const [showScreen, setShowScreen] = useState(false)

    const checkLogin = () => {
        console.log("checkLogin" + global.email)
        if (global.email.length > 0) {
            props.navigation.navigate('Home')
        } else {
            setShowScreen(true)
        }
    }

    const getAccount = async () => {
        console.log("getAccount")
        try {
            const value = await AsyncStorage.getItem('email');
            if (value !== null) {
                // We have data!!
                global.email = value;
                console.log("token " + value)
                checkLogin();
            } else {
                setShowScreen(true)
            }

        } catch (error) {
            setShowScreen(true)
            // Error retrieving data
            console.log(error)
        }
    };

    const login = async() => {
        try {
            await AsyncStorage.setItem('email', global.email);
        } catch (error) {
            // Error saving data
        }
        var resetAction = CommonActions.reset({
            index: 0,
            routes: [
                { name: 'Home' },
            ]
        })
        props.navigation.dispatch(resetAction);
    }

    useEffect(() => {
        getAccount()
    }, [])

    const handleFacebookLogin = () => {
        LoginManager.logInWithPermissions(['public_profile', 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    console.log('Login cancelled')
                } else {
                    console.log('Login success with permissions: ' + result.grantedPermissions.toString())
                    console.log('resultData', result)
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            console.log(data.accessToken.toString())
                            let req = new GraphRequest('/me', {
                                httpMethod: 'GET',
                                version: 'v2.5',
                                parameters: {
                                    'fields': {
                                        'string' : 'email,name,friends'
                                    }
                                }
                            }, (err, res) => {
                                console.log(err, res);
                                if (res!=null){
                                    global.email = res.email
                                    login();
                                }
                            });
                            new GraphRequestManager().addRequest(req).start();
                        }
                    )
                }
            },
            function (error) {
                console.log('Login fail with error: ' + error)
            }
        )
    }

    return (
        <View style={styles.container}>
            {showScreen ? <View style={{flex:1}}>
                <Image style={styles.imageBackground} source={require('../icon/bg.png')} />
                <Image style={styles.logo} source={require('../icon/logo.png')} />
                <View style={styles.bottom}>
                    <JITButton
                        style={styles.button}
                        onPress={() => props.navigation.navigate('SignUp')}
                        title="Sign Up" />
                    <TouchableOpacity
                        style={styles.buttonFacebook}
                        onPress={handleFacebookLogin}>
                        <Image style={styles.iconFacebook} source={require('../icon/icon_facebook.png')} />
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <Text style={styles.textButton}>Sign in with Facebook</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableWithoutFeedback
                        onPress={() => props.navigation.navigate('SignIn')}>
                        <Text style={styles.text}>Already have account - Skip</Text>
                    </TouchableWithoutFeedback>
                </View></View> : null}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imageBackground: {
        flex: 1,
        width: '100%',
        resizeMode: 'cover'
    },
    logo: {
        marginTop: 145,
        width: 125,
        height: 125,
        alignSelf: 'center',
        position: 'absolute',
    },
    bottom: {
        flex: 1,
        bottom: 20,
        position: 'absolute',
        alignSelf: 'center'
    },
    text: {
        color: 'white',
        marginTop: 30,
        alignSelf: 'center',
        fontFamily: 'OpenSans-Light'
    },
    iconFacebook: {
        width: 7,
        height: 15,
        marginStart: 20
    },
    buttonFacebook: {
        width: screenWidth - 50,
        height: 50,
        padding: 12,
        backgroundColor: '#3B5998',
        marginTop: 15,
        flexDirection: 'row',
        alignItems: 'center',
    },
    textButton: {
        color: 'white',
        fontSize: 18,
        fontFamily: "OpenSans-Semibold"
    }

})

export default WelcomComponent;
