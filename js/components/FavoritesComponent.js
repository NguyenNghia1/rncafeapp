import React, { Component } from 'react';
import { View, Text, StatusBar, StyleSheet, Image, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import detailCategoryData from '../data/detailCategoryData';

const screenWidth = Dimensions.get('window').width;
const FavoritesComponent = (props) => {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" hidden={false} backgroundColor='transparent' translucent={true} />
            <View style={styles.header}>
                <TouchableOpacity onPress={() => this.props.navigation.openDrawer()} activeOpacity={1}>
                    <Image style={styles.iconMenu} source={require('../icon/icon_navigation.png')} onPre />
                </TouchableOpacity>
                <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
                    <Text style={styles.textTitle}>Likes</Text>
                </View>
            </View>
            <View style={styles.body}>
                <FlatList
                    data={detailCategoryData}
                    renderItem={({ item }) => (
                        <View style={{ width: (screenWidth / 2 - 15), height: (screenWidth / 2 + 25), marginStart: 10, marginBottom: 10, padding: 10, backgroundColor: 'white' }}>
                            <Text style={{ color: '#666666', fontSize: 16, fontFamily: 'OpenSans-Semibold' }}>{item.name}</Text>
                            <Text style={{ color: '#919191', fontSize: 12, fontFamily: 'OpenSans-Light' }}>{item.detail}</Text>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <Image style={{ width: "65%", height: "65%", alignSelf: 'center', resizeMode: 'center' }} source={require('../icon/choco_frappe.png')} />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: '#0D9F67', fontSize: 14, fontFamily: 'OpenSans-Semibold' }}>${item.price}</Text>
                                <Image style={{ width: 15, height: 15, resizeMode: 'center' }} source={require('../icon/icon_heart_red.png')} />
                            </View>
                        </View>
                    )}
                    //Setting the number of column
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F3F3F3'
    },
    header: {
        width: '100%',
        height: 80,
        flexDirection: 'row',
    },
    iconMenu: {
        width: 20,
        height: 14,
        marginTop: 35,
        marginStart: 17
    },
    textTitle: {
        color: '#666666',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    body: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    }
})

export default FavoritesComponent;