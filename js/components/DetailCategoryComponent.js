import React, { useEffect } from 'react';
import { View, Text, StyleSheet, StatusBar, Image, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import APIGetProducts from '../api/APIGetProducts';

const screenWidth = Dimensions.get('window').width;
const DetailCategoryComponent = (props) => {

    const { goBack } = props.navigation;
    const [callProductsAPI, loading, productResponse, errorMessage] = APIGetProducts();

    useEffect(() => {
        callProductsAPI()
    }, [])

    return (
        <View style={styles.container}>
            <StatusBar barStyle="light-content" hidden={false} backgroundColor='transparent' translucent={true} />
            <View style={styles.up}>
                <Image style={styles.imageBackground} source={require('../icon/bg_image_category.png')} />
                <View style={styles.header}>
                    <TouchableOpacity style={styles.containerIcon} activeOpacity={0.7} onPress={() => goBack()}>
                        <Image style={styles.imageBack} source={require('../icon/icon_back.png')} />
                    </TouchableOpacity>
                    <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
                        <Text style={styles.textTitle}>Coffee</Text>
                    </View>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 50 }}>
                    <Text style={{ color: 'white', fontSize: 24, fontFamily: 'OpenSans-BoldItalic' }}> The Science Of Delicious. </Text>
                    <Text style={{ color: 'white', fontSize: 12, fontFamily: 'OpenSans-Italic' }}> Amazing coffees from around the world! </Text>
                </View>
            </View>
            <View style={styles.down}>
                <FlatList
                    data={productResponse.data}
                    renderItem={({ item }) => (
                        <TouchableOpacity onPress={() => props.navigation.navigate('ProductDetail', { product: item })}>
                            <View style={{ width: (screenWidth / 2 - 15), height: (screenWidth / 2 + 25), marginStart: 10, marginTop: 10, padding: 10, backgroundColor: 'white' }}>
                                <Text style={{ color: '#666666', fontSize: 16, fontFamily: 'OpenSans-Semibold' }}>{item.name}</Text>
                                <Text style={{ color: '#919191', fontSize: 12, fontFamily: 'OpenSans-Light' }}>{item.slug}</Text>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image style={{ width: "65%", height: "65%", alignSelf: 'center', resizeMode: 'center' }} source={{ uri: item.images[0].src }} />
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ color: '#0D9F67', fontSize: 14, fontFamily: 'OpenSans-Semibold' }}>${item.price}</Text>
                                    <Image style={{ width: 15, height: 15, resizeMode: 'center' }} source={require('../icon/icon_heart.png')} />
                                </View>
                            </View></TouchableOpacity>
                    )}
                    //Setting the number of column
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    up: {
        flex: 1,
    },
    down: {
        flex: 2,
        marginBottom: 10,
        backgroundColor: '#F3F3F3'
    },
    imageBackground: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        position: 'absolute'
    },
    header: {
        width: '100%',
        height: 65,
        flexDirection: 'row',
        position: 'absolute'
    },
    imageBack: {
        width: 10,
        height: 17,
    },
    textTitle: {
        color: 'white',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    containerIcon: {
        width: 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
    }
})

export default DetailCategoryComponent;
