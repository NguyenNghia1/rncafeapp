import React, { useEffect } from 'react';
import { View, Text, StatusBar, StyleSheet, Image, TouchableOpacity, Dimensions, FlatList, TouchableWithoutFeedback } from 'react-native';
import categoryData from '../data/categoryData';
import { DrawerActions } from '@react-navigation/native';
import APIGetCategories from '../api/APIGetCategories'
import Spinner from 'react-native-loading-spinner-overlay';

const screenWidth = Dimensions.get('window').width;
const HomeComponent = (props) => {
    const [callCategoryAPI, loading, categoriesResponse, errorMessage] = APIGetCategories();

    useEffect(() => {
        callCategoryAPI()
    }, [])

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" hidden={false} backgroundColor='transparent' translucent={true} />
            <View style={styles.header}>
                <TouchableOpacity style={styles.containerIcon} onPress={() => props.navigation.dispatch(DrawerActions.openDrawer())} activeOpacity={0.7}>
                    <Image style={styles.iconMenu} source={require('../icon/icon_navigation.png')} />
                </TouchableOpacity>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text style={styles.textTitle}>Menu</Text>
                </View>
                <TouchableOpacity style={styles.containerIcon} onPress={() => props.navigation.navigate('ShoppingCart')} activeOpacity={0.7}>
                    <Image style={styles.imageCart} source={require('../icon/icon_cart.png')} />
                </TouchableOpacity>
            </View>
            <View style={styles.body}>
                <FlatList
                    data={categoriesResponse.data}
                    renderItem={({ item }) => {
                        return (
                            <TouchableWithoutFeedback onPress={() => props.navigation.navigate('DetailCategoryComponent')}>
                                <View style={{ marginStart: 10, width: (screenWidth / 2 - 15), marginBottom: 10, backgroundColor: 'white' }}>
                                    {item.image != null ?
                                        <Image style={{ width: '100%', height: (screenWidth / 3), resizeMode: 'cover' }} source={{ uri: item.image.src }} /> :
                                        <Image style={{ width: '100%', height: (screenWidth / 3), resizeMode: 'cover' }} source={require('../../assets/no-image.jpg')} />}
                                    <View style={{ paddingBottom: 15 }}>
                                        <Text style={{
                                            color: '#666666',
                                            alignItems: 'center',
                                            fontFamily: 'OpenSans-Semibold',
                                            fontSize: 16,
                                            marginTop: 15,
                                            marginStart: 15
                                        }}>{item.name}</Text>
                                        <Text style={{
                                            color: '#666666', alignItems: 'center', fontFamily: 'OpenSans-Light', fontSize: 12, marginTop: 5, marginStart: 15
                                        }}>{item.slug}</Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        )
                    }}
                    //Setting the number of column
                    numColumns={2}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
            <Spinner 
                visible={loading}
                textStyle={{ color: '#FFF' }}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        width: '100%',
        height: 80,
        flexDirection: 'row',
        backgroundColor: 'white',
        paddingTop: 30,
    },
    iconMenu: {
        width: 20,
        height: 14,
    },
    textTitle: {
        color: '#666666',
        textAlign: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 5
    },
    body: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    imageCart: {
        width: 30,
        height: 30,
    },
    containerIcon: {
        width: 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center'
    }

})

export default HomeComponent;
