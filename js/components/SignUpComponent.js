import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Dimensions, Keyboard, ScrollView, TouchableWithoutFeedback, Alert } from 'react-native';
import { CheckBox } from 'react-native-elements'
import JITTextInput from '../common/JITTextInput';
import JITButon from '../common/JITButton';
import JITCkeckBox from '../common/JITCheckBox';
import UTILS from '../Utils'

const SignUpComponent = props => {

    const [fullnameStr, setFullName] = useState('')
    const [emailStr, setEmail] = useState('')
    const [pw, setPW] = useState('')
    const [confirmPW, setConfirmPW] = useState('')
    const [isChecked, setChecked] = useState(false)

    const validate = () => {
        if (fullnameStr.trim().length == 0) {
            alert('Xin bạn vui lòng nhập tên')
            return false;
        } else if (emailStr.trim().length == 0) {
            alert('Xin bạn vui lòng nhập email')
            return false;
        } else if (!UTILS.ValidateEmail(emailStr)) {
            alert('Email bạn nhập không hợp lệ')
            return false;
        } else if (pw.trim().length == 0) {
            alert('Xin bạn vui lòng nhập mật khẩu')
            return false;
        } else if (confirmPW != pw) {
            alert('Mật khẩu nhập lại không đúng')
            return false;
        } else if (!isChecked) {
            alert('Bạn chưa đồng ý với điều khoản sử dụng')
            return false;
        }

        return true;
    }

    const signUp = () => {
        if (validate()) {
            Alert.alert(
                "",
                "Đăng ký thành công",
                [
                    { text: "OK", onPress: () => props.navigation.goBack() }
                ],
                { cancelable: false }
            );
        }
    }

    return (
        <ScrollView contentContainerStyle={styles.contentContainerScroll}
            style={styles.scroll}>
            <View style={styles.container}>
                <StatusBar barStyle="light-content" hidden={false} backgroundColor='transparent' translucent={true} />
                <View style={styles.header}>
                    <TouchableWithoutFeedback style={styles.containerIcon} onPress={() => { props.navigation.goBack() }}><Image style={styles.imageBack} source={require('../icon/icon_back.png')} /></TouchableWithoutFeedback>
                    <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
                        <Text style={styles.textTitle}>Sign Up</Text>
                    </View>
                </View>
                <View style={styles.avartar} >
                    <Image style={styles.imageAvatar} source={require('../icon/icon_cam.png')} />
                </View>
                <View style={{ height: 1, backgroundColor: '#E6E9ED', marginTop: 55 }} />
                <JITTextInput
                    title="Full Name"
                    returnKeyType={"next"}
                    onSubmitEditing={() => { email.focus() }}
                    blurOnSubmit={false}
                    onChangeText={text => setFullName(text)} />
                <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
                <JITTextInput
                    inputRef={(r) => { email = r }}
                    title="Email"
                    returnKeyType={"next"}
                    onSubmitEditing={() => { password.focus() }}
                    blurOnSubmit={false}
                    onChangeText={text => setEmail(text)} />
                    <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
                <JITTextInput
                    inputRef={(r) => { password = r }}
                    title="Password"
                    returnKeyType={"next"}
                    onSubmitEditing={() => { confirmPassword.focus() }}
                    blurOnSubmit={false}
                    onChangeText={text => setPW(text)}
                    secureTextEntry={true} />
                    <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
                <JITTextInput
                    inputRef={(r) => { confirmPassword = r }}
                    title="Confirm Password"
                    onChangeText={text => setConfirmPW(text)}
                    secureTextEntry={true} />
                    <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
                {/* <JITCkeckBox
                    titleCheckbox="I agree to the Terms Conditions" /> */}
                <CheckBox
                    containerStyle={{ backgroundColor: 'white', borderColor: 'white' }}
                    textStyle={{ color: '#919191', fontFamily: 'OpenSans-Light' }}
                    title="I agree to the Terms Conditions"
                    checked={isChecked}
                    onPress={() => setChecked(!isChecked)} />
            </View>
            <View style={styles.bottom}>
                <JITButon
                    style={styles.button}
                    title="Sign Up"
                    onPress={signUp} />
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    contentContainerScroll: {
        flexGrow: 1,
        justifyContent: 'space-between',
        flexDirection: 'column'
    },
    scroll: {
        backgroundColor: 'white',
        paddingBottom: 0
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    },
    header: {
        width: '100%',
        height: 65,
        backgroundColor: '#DBC8A8',
        flexDirection: 'row',
    },
    avartar: {
        width: 100,
        height: 100,
        marginTop: 55,
        borderRadius: 100 / 2,
        backgroundColor: '#E3E3E3',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageAvatar: {
        height: 35,
        width: 35,
        resizeMode: 'cover'
    },
    imageBack: {
        width: 10,
        height: 17,
    },
    textTitle: {
        color: 'white',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    text: {
        color: '#919191',
        fontFamily: 'OpenSans-Light'
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        bottom: 0,
    },
    button: {
        width: '100%',
        height: 50,
        padding: 12,
        backgroundColor: '#0D9F67',
        marginTop: 3
    },
    containerIcon: {
        width: 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
    }
})

export default SignUpComponent;
