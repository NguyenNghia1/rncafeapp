import React, { useEffect, useState, Fragment } from 'react';
import { View, Text, FlatList, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import JITCheckBox from '../common/JITCheckBox';
import JITButon from '../common/JITButton';
import cartItem from '../data/cartItem';

const screenWidth = Dimensions.get('window').width
let newCheckItems = [];
const ShoppingCardComponent = props => {
    const [deleteRowKey, setDeleteRowKey] = useState(null)
    const [checkAll, setCheckAll] = useState(true)
    const [checkItems, setCheckItems] = useState([])
    const { goBack } = props.navigation;

    useEffect(() => {
        for (i = 0; i < cartItem.length; i++) {
            newCheckItems.push(true)
        }
        setCheckItems(newCheckItems)
    }, [])

    const updateCheckedItem = () => {
        for (i = 0; i < newCheckItems.length; i++) {
            newCheckItems[i] = !checkAll;
        }
        setCheckItems(newCheckItems)
    }

    const refreshFlatList = (deleteKey) => {
        setDeleteRowKey(deleteKey)
    }
    return (
        <View style={{
            flex: 1,
            backgroundColor: 'white',
        }}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.containerIcon} onPress={() => goBack()}>
                    <Image style={styles.imageBack} source={require('../icon/icon_back_dark.png')} />
                </TouchableOpacity>
                <View style={{ flex: 1, alignItems: 'center', paddingEnd: 40 }}>
                    <Text style={styles.textTitle}>Giỏ Hàng</Text>
                </View>
            </View>
            <View style={{ backgroundColor: 'white', flex:1}}>
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}>
                    <View style={{
                        paddingTop: 10,
                        flexDirection: 'row'
                    }}>
                        <JITCheckBox
                            unCheckedStyle={styles.unCheckedStyle}
                            checked={checkAll}
                            onPress={() => {
                                setCheckAll(!checkAll)
                                updateCheckedItem()
                            }}
                        />
                        <Image style={{ width: 15, height: 15, marginEnd: 5, alignSelf: 'center' }} source={{ uri: `http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_1_front.jpg` }} />
                        <Text style={styles.text}>nguyenn</Text>
                    </View>
                    <JITButon
                        style={{ width: 100, height: 30, backgroundColor: '#0D9F67', borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}
                        textStyle={{ fontSize: 14, color: 'white' }}
                        title='Mua hàng'
                        onPress={() => {
                            let slectedItem = [];
                            for (i = 0; i < checkItems.length; i++) {
                                if (checkItems[i] === true) {
                                    slectedItem.push(cartItem[i])
                                }
                            }
                            props.navigation.navigate('ShippingInfo', { products: slectedItem })
                        }}
                    />
                </View>

                <FlatList
                style={{flexGrow:1}}
                    data={cartItem}
                    renderItem={({ item, index }) => (
                        <View>
                            <View style={{ height: 1, backgroundColor: '#E6E9ED'}} />
                            <View style={{
                                flexDirection: 'row',justifyContent:'center',alignItems:'center'
                            }}>
                                <JITCheckBox
                                    unCheckedStyle={styles.unCheckedStyle}
                                    checked={checkItems[index]}
                                    onPress={() => {
                                        newCheckItems = [...checkItems]
                                        newCheckItems[index] = !newCheckItems[index]
                                        setCheckItems(newCheckItems)
                                        setCheckAll(false)
                                    }}
                                />
                                <Image style={{ width: 70, height: 70, marginEnd: 10}} source={{ uri: item.uri }} />
                                <View style={{flexGrow:1}}>
                                    <Text
                                        style={{
                                            fontSize: 12,
                                            color: '#666666',
                                            width: screenWidth * 0.4,
                                            fontFamily: 'OpenSans-Light',
                                        
                                        }}
                                        numberOfLines={1}>Tên sản phẩm: {item.name}</Text>
                                    <Text style={{
                                        fontSize: 12,
                                        color: '#919191',
                                        fontFamily: 'OpenSans-Light',
                                    }}>Phân Loại: {item.size}</Text>
                                    <Text style={{
                                        fontSize: 12,
                                        color: '#0D9F67',
                                        fontFamily: 'OpenSans-Semibold',
                                        marginTop: 15
                                    }}>{item.price} x {item.quantity}</Text>
                                </View>
                                <JITButon
                                    style={{ flexGrow: 1, alignItems: 'center', width: 15, height: 100, backgroundColor: '#0D9F67'}}
                                    title="Xoá"
                                    textStyle={{ fontSize: 12 }}
                                    onPress={() => {
                                        cartItem.splice(index, 1)
                                        refreshFlatList(item.product_id)
                                    }}
                                />
                            </View>
                        </View>
                    )}
                    keyExtractor={item => item.id}
                >
                </FlatList>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 65,
        flexDirection: 'row',
    },
    imageBack: {
        width: 10,
        height: 17,
    },
    textTitle: {
        color: '#666666',
        alignItems: 'center',
        fontFamily: 'OpenSans-Bold',
        fontSize: 18,
        marginTop: 30
    },
    text: {
        fontSize: 12,
        color: '#919191',
        fontFamily: 'OpenSans-Light',
        marginBottom: 5,
        alignSelf: 'center',
    },
    highlightText: {
        fontSize: 12,
        color: '#0D9F67',
        fontFamily: 'OpenSans-Light'
    },
    unCheckedStyle: {
        width: 15,
        height: 15
    },
    button: {
        marginTop: 0, width: 45, height: '100%'
    },
    containerIcon: {
        width: 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25,
    }
})

export default ShoppingCardComponent;
