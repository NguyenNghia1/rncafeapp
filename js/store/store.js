//Redux
import { createStore, applyMiddleware } from 'redux';
//Redux saga
import createSagaMiddleware from 'redux-saga';
import rootSaga from './js/sagas/rootSaga';