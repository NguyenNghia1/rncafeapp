import { NativeModules } from "react-native";

var cardItem = [
    {
        "product_id": 26,
        "name": "Áo thun sơ mi ngắn tay",
        "size": "M",
        "quantity": 2,
        "price": 48.2,
        "uri": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg"

    },
    {
        "product_id": 29,
        "name": "Áo unisex nam nữ mùa hè mãu mới",
        "size": "L",
        "quantity": 1,
        "price": 23.32,
        "uri": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_1_front.jpg"

    },
    {
        "product_id": 66,
        "name": "Áo phông ngắn tay hoạ tiết đẹp",
        "size": "M",
        "quantity": 3,
        "price": 48.2,
        "uri": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_3_front.jpg"

    },
    {
        "product_id": 63,
        "name": "Áo ngắn tay thêu hoa sang chảnh",
        "size": "XXL",
        "quantity": 3,
        "price": 48.2,
        "uri": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_4_front.jpg"

    },
    {
        "product_id": 61,
        "name": "Áo sơ mi caro ôm dáng vải mát",
        "size": "XL",
        "quantity": 3,
        "price": 29.2,
        "uri": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_5_front.jpg"

    },
    {
        "product_id": 60,
        "name": "Áo sơ mi caro ôm dáng vải mát",
        "size": "XL",
        "quantity": 3,
        "price": 28.2,
        "uri": "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_5_front.jpg"

    }
];

module.exports = cardItem;