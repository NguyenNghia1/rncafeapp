import { NativeModules } from "react-native";

var categoryData = [
    {
        "id": 1,
        "name": "Coffee",
        "detail": "Freshly brewed coffee",
        "path": "coffee_menu.png"
    },
    {
        "id": 2,
        "name": "Breakfast",
        "detail": "Hearty hot & full flavour",
        "path": "breakfast_menu.png"
    },
    {
        "id": 3,
        "name": "Munchies",
        "detail": "Perfectly baked goodies",
        "path": "munchies_menu.png"
    },
    {
        "id": 4,
        "name": "Sandwiches",
        "detail": "Fresh healthy and tasty",
        "path": "sandwiches_menu.png"
    },
    {
        "id": 5,
        "name": "Sandwiches",
        "detail": "Fresh healthy and tasty",
        "path": "tea_menu.png"
    },
    {
        "id": 6,
        "name": "Dinner",
        "detail": "Fresh healthy and tasty",
        "path": "dinner_menu.png"
    },
];

module.exports = categoryData;