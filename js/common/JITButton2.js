import React from 'react';
import { TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';

const JITButon = props => {
    return (
        <TouchableOpacity
            style={{...styles.button,...props.style}}
            onPress={props.onPress}
            activeOpacity={0.8}>
            <Text style={{...styles.butonText, ...props.textStyle}}>{props.title}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    butonText: {
        color: 'white',
        fontSize: 18,
        fontFamily: "OpenSans-Semibold",
        textAlign: 'center'
    },
    button: {
        height: 50,
        alignItems:"center",
        justifyContent: 'center',
        backgroundColor: '#0D9F67'
    }
})

export default JITButon