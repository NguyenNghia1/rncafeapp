import React from 'react';
import { View, Switch, Text } from 'react-native';

const JITSwitch = props => {
    return (
        <View>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginStart: 15,
                    marginEnd: 15,
                    height: 62
                }}>
                <Text
                    style={{
                        color: '#919191',
                        fontFamily: 'OpenSans-Light',
                        fontSize: 16
                    }}>{props.title}</Text>
                <Switch
                    value={props.value}
                />
            </View>
            <View style={{ height: 1, backgroundColor: '#E6E9ED' }} />
        </View>
    )
}

export default JITSwitch