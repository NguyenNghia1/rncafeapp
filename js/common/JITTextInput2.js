import React from 'react';
import { TouchableOpacity, Text, StyleSheet, TextInput, View, Keyboard } from 'react-native';

const JITTextInput2 = ({ inputRef, ...props }) => {
  return (
    <View>
        {/* <Text style={styles.text}>{props.title}</Text> */}
        <TextInput
          ref={(r) => { inputRef && inputRef(r) }}
          maxLength={40}
          underlineColorAndroid="transparent"
          placeholder={props.title}
          keyboardType={props.keyboardType}
          onChangeText={props.onChangeText}
          returnKeyType={props.returnKeyType}
          numberOfLines={props.numberOfLines}
          multiline={props.multiline}
          onSubmitEditing={props.onSubmitEditing}
          autoFocus={props.autoFocus}
          blurOnSubmit={props.blurOnSubmit}
          secureTextEntry={props.secureTextEntry}
          style={props.style}
          value={props.value}
          editable={props.editable} />
    </View>
  );
}

export default JITTextInput2;