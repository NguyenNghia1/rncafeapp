import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { CheckBox } from 'react-native-elements'

const JITCheckBox = (props) => {
    return (
        <View style={styles.container}>
            <View style={{
                flexDirection: 'row',
                alignItems: 'center'
            }}>
                <CheckBox
                    uncheckedColor='#E5E8EC'
                    uncheckedIcon={<View style={{ ...styles.unCheckedStyle, ...props.unCheckedStyle }} />}
                    checkedIcon={<Image style={{ ...styles.checkedStyle, ...props.checkedStyle }} source={require('../icon/icon_checked.png')} />}
                    checked={props.checked}
                    onPress={props.onPress}
                />
                <Text style={{ ...styles.textTitle, ...props.style }}>{props.titleCheckbox}</Text>
            </View>
            <View>
                <Text style={{
                    fontSize: 16,
                    fontFamily: 'OpenSans-Semibold',
                    color: '#666666'
                }}>{props.text}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    unCheckedStyle: {
        width: 20,
        height: 20,
        borderWidth: 1,
        borderColor: '#E5E8EC'
    },
    checkedStyle: {
        width: 15,
        height: 15,
        resizeMode: 'cover'
    },
    textTitle: {
        color: '#919191',
        fontFamily: 'OpenSans-Light',
    }
})

export default JITCheckBox;