import React from 'react';
import { TouchableOpacity, Text, StyleSheet, TextInput, View, Keyboard } from 'react-native';

const JITTextInput = ({ inputRef, ...props }) => {
  return (
    <View>
      <TouchableOpacity
        style={styles.button}
        onPress={props.onPress}
        activeOpacity={1}
        disabled={props.disabled}
      >
        {/* <Text style={styles.text}>{props.title}</Text> */}
        <TextInput
          ref={(r) => { inputRef && inputRef(r) }}
          maxLength={40}
          position='absolute'
          underlineColorAndroid="transparent"
          placeholder={props.title}
          keyboardType={props.keyboardType}
          onChangeText={props.onChangeText}
          returnKeyType={props.returnKeyType}
          numberOfLines={props.numberOfLines}
          multiline={props.multiline}
          onSubmitEditing={props.onSubmitEditing}
          autoFocus={props.autoFocus}
          blurOnSubmit={props.blurOnSubmit}
          secureTextEntry={props.secureTextEntry}
          style={{...styles.textInput,...props.style}}
          value={props.value}
          editable={props.editable} />

      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  text: {
    color: '#919191',
    fontFamily: 'OpenSans-Light',
    fontSize: 16
  },
  textInput: {
    width: '100%',
    color: '#919191',
    fontFamily: 'OpenSans-Light',
    fontSize: 16
  },
  button: {
    flexDirection: 'row',
    marginStart: 15,
    marginEnd: 15,
    alignItems: "center",
    justifyContent: 'space-between',
    height: 62
  }
})

export default JITTextInput;