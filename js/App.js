import React, { Component } from 'react';
import {AsyncStorage } from 'react-native';
//Navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import CustomDrawer from './components/custom/CustomDrawer'
//Redux
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';

import WelcomComponent from './components/WelcomComponent';
import SignInComponent from './components/SignInComponent';
import SignUpComponent from './components/SignUpComponent';
import UserProfileComponent from './components/UserProfileComponent';
import OrderSummaryComponent from './components/OrderSummaryComponent';
import DetailCategoryComponent from './components/DetailCategoryComponent';
import ShippingInfoComponent from './components/checkout/ShippingInfoComponent';
import ProcessingOrderComponent from './components/orderstatus/ProsessingOrderComponent';
import ShoppingCardComponent from './components/ShoppingCardComponent';
import ProductDetailComponent from './components/ProductDetailComponent';
import OrderDetailComponent from './components/OrderDetailComponent';

global.email = '';

const Stack = createStackNavigator();

console.disableYellowBox = true;
const App = () => {
    return (
      <NavigationContainer>
        <Stack.Navigator headerMode={'none'}
        initialRouteName="Welcome" >
          <Stack.Screen name="Welcome" component={WelcomComponent} />
          <Stack.Screen name="SignIn" component={SignInComponent} />
          <Stack.Screen name="SignUp" component={SignUpComponent} />
          <Stack.Screen name="Home" component={CustomDrawer}/>
          <Stack.Screen name="Category" component={DetailCategoryComponent}/>
          <Stack.Screen name="DetailCategoryComponent" component={DetailCategoryComponent}/>
          <Stack.Screen name="ProductDetail" component={ProductDetailComponent}/>
          <Stack.Screen name="Profile" component={UserProfileComponent}/>
          <Stack.Screen name="SummaryOrder" component={OrderSummaryComponent} />
          <Stack.Screen name="ShippingInfo" component={ShippingInfoComponent} />
          <Stack.Screen name="ShoppingCart" component={ShoppingCardComponent} />     
          <Stack.Screen name="OrderDetail" component={OrderDetailComponent} />
          <Stack.Screen name="ProcessingOrder" component={ProcessingOrderComponent} />
        </Stack.Navigator>
      </NavigationContainer>
    );
}

export default App;
